package com.almundo.managedBean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.hibernate.Session;

import com.almundo.model.Director;
import com.almundo.model.Employee;
import com.almundo.model.Operator;
import com.almundo.model.Supervisor;
import com.almundo.utils.HibernateUtil;

@ManagedBean
public class EmployeeManagedBean {
	
	private int id;
	private String name;
	private String lastName;
	protected int age;
	private String gender;
	private boolean available;
	private String type_employee;
	private List<Employee> listEmployees;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	public String getType_employee() {
		return type_employee;
	}
	public void setType_employee(String type_employee) {
		this.type_employee = type_employee;
	}
	
	public List<Employee> getListEmployees() {
		return listEmployees;
	}
	
	public void setListEmployees(List<Employee> listEmployees) {
		this.listEmployees = listEmployees;
	}
	@PostConstruct
	public void init() {
        listEmployees = getEmployeeList();
    }
	
	// This method return a list of employees
	public static List<Employee> getEmployeeList(){
		ArrayList<Employee> listEmployees = new ArrayList<Employee>();
		
		
		for (int i=0; i<5; i++){
			if (i<5){
				Employee employee = new Operator();
				employee.setName("Operator " + i+1);
				employee.setLastName("");
				employee.setAge(24);
				employee.setType_employee("Operator");
				employee.setAvailable(true);
				listEmployees.add(employee);
				
			}else if(i<10){
				Employee employee = new Supervisor();
				employee.setName("Supervisor " + i+1);
				employee.setLastName("");
				employee.setAge(24);
				employee.setType_employee("Supervisor");
				employee.setAvailable(true);
				listEmployees.add(employee);

			}else{
				Employee employee = new Director();
				employee.setName("Director " + i+1);
				employee.setLastName("");
				employee.setAge(24);
				employee.setType_employee("Director");
				employee.setAvailable(true);
				listEmployees.add(employee);
				
			}
			
			
		}
		
		return listEmployees;
	}
	
	/*public void saveEmployee(){
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Employee employee = new Operator();
		employee.setName(name);
		employee.setLastName(lastName);
		employee.setAge(24);
		employee.setType_employee(type_employee);
		employee.setAvailable(false);
		session.save(employee);
		session.getTransaction().commit();
		session.close();
	}*/

}
