package com.almundo.model;

import java.util.List;

import com.almundo.managedBean.EmployeeManagedBean;

public class Dispatcher {
	
	public Dispatcher() {
			// TODO Auto-generated constructor stub
		}
	
	public static List<Call> calls;

	// This method calculates the time of the call

	public static void timeCall() {
		int seconds = (int) (Math.random() * 10) + 5;
		if (seconds > 10) {
			seconds -= 5;
		}
		long timeInicio, diference = 0;
		timeInicio = System.currentTimeMillis();
		while ((int) diference < seconds) {
			System.out.println("calling");
			diference = System.currentTimeMillis() - timeInicio;
		}

		System.out.println("Call Finished");
	}
	// This method assigns a call to an available employee
	public static String dispatchCall(Call call) {
		calls.add(call);
		if (calls.size()>10){
			return "wait a moment on the line to be attended";
		}

		for (Employee employee : EmployeeManagedBean.getEmployeeList()) {
			if (employee.getType_employee() == "Operator" && employee.isAvailable()) {
				call.setStautusCall("Atended");
				employee.setAvailable(false);
				timeCall();
				employee.setAvailable(true);
				calls.remove(call);
				return "Call Atended";
			}
		}

		for (Employee employee : EmployeeManagedBean.getEmployeeList()) {
			if (employee.getType_employee() == "Supervisor" && employee.isAvailable()&& call.getStautusCall() == "Not Atended") {
				call.setStautusCall("Atended");
				employee.setAvailable(false);
				timeCall();
				employee.setAvailable(true);
				calls.remove(call);
				return "Call Atended";
			}
		}
		
		for (Employee employee : EmployeeManagedBean.getEmployeeList()) {
			if (employee.getType_employee() == "Direcor" && employee.isAvailable()&& call.getStautusCall() == "Not Atended") {
				call.setStautusCall("Atended");
				employee.setAvailable(false);
				timeCall();
				employee.setAvailable(true);
				calls.remove(call);
				return "Call Atended";
			}
		}
		
		return "At this time we can not answer your call please wait on the line or call later";
	}

}
