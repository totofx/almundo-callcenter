package callCenter;

import static org.junit.Assert.*;

import org.junit.Test;

import com.almundo.model.Call;
import com.almundo.model.Dispatcher;

public class DispatcherTest {
	//Units Tests
	@Test
	public void dispatcherCalltest(){
		Dispatcher dispatcher = new Dispatcher();
		for(int i = 0; i < 10; i++){
			dispatcher.calls.add(new Call());
		}
		
		assertEquals("wait a moment on the line to be attended", dispatcher.dispatchCall(new Call()));
	}

}
